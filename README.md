madrone: a cool tree with reasonable defaults

# System requirements
* [tree](http://mama.indstate.edu/users/ice/tree/)

# Suggested installation

    sudo install md /usr/local/bin

# Usage

    $ md
    .
    ├── .hidden_file_that_you_want_to_see
    ├── madrone
    └── README.md
    
    0 directories, 3 files
    
## ...as opposed to...

    $ tree -a
    .
    ├── .git
    │   ├── branches
    │   ├── COMMIT_EDITMSG
    │   ├── config
    │   ├── description
    │   ├── FETCH_HEAD
    │   ├── HEAD
    │   ├── hooks
    │   │   ├── applypatch-msg.sample
    │   │   ├── commit-msg.sample
    │   │   ├── post-update.sample
    │   │   ├── pre-applypatch.sample
    │   │   ├── pre-commit.sample
    │   │   ├── prepare-commit-msg.sample
    │   │   ├── pre-push.sample
    │   │   ├── pre-rebase.sample
    │   │   └── update.sample
    │   ├── index
    │   ├── info
    │   │   └── exclude
    │   ├── logs
    │   │   ├── HEAD
    │   │   └── refs
    │   │       ├── heads
    │   │       │   └── master
    │   │       └── remotes
    │   │           └── origin
    │   │               └── master
    │   ├── objects
    │   │   ├── 48
    │   │   │   └── 20039c19df08e996f1769e86f77586f11ac30b
    │   │   ├── 7b
    │   │   │   └── 98f02a512c27b4f01fd612e575ac38c626338d
    │   │   ├── 84
    │   │   │   └── c44d725b8cdfe0299c90299d4be909bef082e6
    │   │   ├── 93
    │   │   │   └── 97b9edb3656afb7bee7240506dbc6bd2c2f310
    │   │   ├── a0
    │   │   │   └── a5e8a5f744027ba8408d28850f9450e435281c
    │   │   ├── db
    │   │   │   └── 88598b8400e2b4c2451a3fe2f0349e78f8ed62
    │   │   ├── f3
    │   │   │   └── edf46fd01f442c5c5d80a0cc1e96af002b778e
    │   │   ├── info
    │   │   └── pack
    │   ├── ORIG_HEAD
    │   └── refs
    │       ├── heads
    │       │   └── master
    │       ├── remotes
    │       │   └── origin
    │       │       └── master
    │       └── tags
    ├── .hidden_file_that_you_want_to_see
    ├── madrone
    ├── node_modules
    │   └── vendored_file_you_totally_cannot_see
    └── README.md
    
    25 directories, 33 files

## Defaults
- ignores folders that you're probably not interested in:
  - `.git`
  - `node_modules`
- includes all other hidden files

## Contribute
Pull request are welcome for more cool default configurations for madrone.